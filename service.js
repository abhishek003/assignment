const mongoose = require('mongoose')
const userSchema = require('./userSchema.js')
const User = mongoose.model('user', userSchema)

module.exports = {
    createUser,
    findUser,
    findUserAndUpdate,
    findUserCredentials
}

async function createUser(userdetails) {
    return new User({
        storagetype: userdetails.storagetype,
        emailid: userdetails.emailid,
        password: userdetails.password,
        phonenumber: userdetails.phonenumber,
        hashid: userdetails.hashid,
        isactive:0,
        created: Date.now()
    }).save()
}

async function findUser(search_emailid) {
    return await User.findOne({ 'emailid' : search_emailid })
}

async function findUserCredentials(details) {
    return await User.findOne({ emailid : details.emailid, password : details.password })
}

async function findUserAndUpdate(query) {
    return await User.findOneAndUpdate(query, { isactive: 1 }, function(err,res){
        console.log(res);
    })
}