const express = require('express');
const exphbs  = require('express-handlebars');
const bodyParser = require('body-parser');
const nodemailer = require("nodemailer");
const SendOtp = require('sendotp');
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs');
/* Loads all variables from .env file to "process.env" */
require('dotenv').config();

const userService = require('./service.js')
const sendOtp = new SendOtp(process.env['OTP_TOKEN']);
const connectionString = process.env['MONGO_URI']
const connector = mongoose.connect(connectionString)
const smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: process.env['AUTH_USER'],
        pass: process.env['AUTH_PWD']
    }
});

// Set up and configure the Express framework
var app = express();
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.use(bodyParser.urlencoded({ extended : true }));
var rand,mailOptions,host,link;

// Starting page GET
app.get('/', function(req, res) {
    res.render('step1');
});

// Starting page POST
app.post('/step1', function(req, res) {
    res.render('step1');
});

// SignIn user with Email OR Mobile OTP
app.post('/step2', async function(req, res) {    
    if(req.body.email!=="" && req.body.email!== null && req.body.password!=="" && req.body.password!==null) {
        let response = await connector.then(async () => {
            return userService.findUser(req.body.email) //return userService.findUserCredentials(details)
        })
        if(bcrypt.compareSync(req.body.password, response.password)){
            console.log('Sign In Successful')
            res.render('step3', {
                value : 'Email'
            });
        } else {
            res.render('step4', {
                error: 'Unable to login',
                text: 'Or User does not exists!'
            });
        }
    }
    else {
        // Make Mobile OTP request send to verify LogIn
        sendOtp.send(req.body.phone, "WHITED", function (error, data) {
            if(data.type == 'success'){
                console.log('OTP Sent Successfully!');
                res.render('step2', {
                    id : req.body.phone
                });
            }
            else{
                res.render('step1', { error : error });
            }
        });
    }
});

// Verify whether Mobile OTP sent token is correct
app.post('/step3', async function(req, res) {
    var id = req.body.id;
    var token = req.body.token;
    sendOtp.verify(id, token, function (error, data) {
        if(data.type == 'success') {
            console.log('OTP verified successfully')
            res.render('step3', {
                value : 'Mobile Number'
            });
        }else{
            console.log('OTP verification failed')
            res.render('step4', { error: error, text: 'Verification code has failed!' });    
        }
    });
});

// Submit the Signup user form
app.post('/step5', async function(req, res) {
    rand=Math.floor((Math.random() * 100) + 100);
    var userdetails ={} 
    userdetails.emailid = req.body.subemailid;
    userdetails.password = bcrypt.hashSync(req.body.subpassword, 10);
    if(req.body.subemailid == "" || req.body.subpassword == "" || req.body.subemailid == null || req.body.subpassword == null) {
        res.end("Please fill username and password first!");
        return
    }
    if(req.body.subemailid !== "" && req.body.subemailid !== null && req.body.subphonenumber !== null && req.body.subphonenumber !== "") { 
        userdetails.storagetype = 'both';
    }else if(req.body.subphonenumber !== null && req.body.subphonenumber !== ""){
        userdetails.storagetype = 'phone'
    }else if(req.body.subemailid !== null && req.body.subemailid !== ""){
        userdetails.storagetype = 'email'
    }
    userdetails.phonenumber = req.body.subphonenumber;
    userdetails.hashid = rand;
    let user = await userService.findUser(userdetails.emailid)
        if (!user) {
        updated = await userService.createUser(userdetails)
        host=req.get('host');
        link="http://"+req.get('host')+"/verify?id="+rand;
        mailOptions={
            to : userdetails.emailid,
            subject : "Please confirm your Email account",
            html : "Hello,<br> Please Click on the link to verify your email.<br><a href="+link+">Click here to verify</a>"
        }
        smtpTransport.sendMail(mailOptions, function(error, response){
            if(error){
                res.end("error");
            }else{
                res.end("sent");
            }
        });
        res.render('step5');
    }
    else {
        console.log('User already exists!')
        res.render('step4', { error: 'User already exists!' });
    }
})

// Verify the Email
app.get('/verify',async function(req,res) {
    if((req.protocol+"://"+req.get('host'))==("http://"+host)) {
        console.log("Domain is matched. Information is from Authentic email");
        if(req.query.id==rand) {
            var query = { hashid: rand };
            updated = await userService.findUserAndUpdate(query)
            console.log("Email is verified");
            res.end("<h1>Email "+mailOptions.to+" is been Successfully verified");
        }
        else {
            console.log("Email is not verified");
            res.end("<h1>Bad Request</h1>");
        }
    }
    else {
        res.end("<h1>Request is from unknown source");
    }
});

// Start the application
app.listen(process.env.PORT || process.env['LOCAL_PORT']);
