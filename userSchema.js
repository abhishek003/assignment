const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({
    storagetype: {
        type: String,
        required: [true, 'Storage type is required']
    },
    emailid: {
        type: String,
        required: [true, 'Storage type is required']
    },
    password: {
        type: String,
        required: [true, 'Storage type is required']
    },
    phonenumber: Number,
    hashid: Number,
    isactive: { 
        type: Number, 
        default: 0 
    },
    created: {
        type: Date,
        required: [true, 'Created date is required']
    }
});

module.exports = userSchema